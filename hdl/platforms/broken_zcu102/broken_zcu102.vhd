library ieee;       use ieee.std_logic_1164.all;
                    use ieee.numeric_std.all;
library ocpi;       use ocpi.types.all;
library platform;   use platform.platform_pkg.all;
library zynq_ultra; use zynq_ultra.zynq_ultra_pkg.all;
library axi;        use axi.axi_pkg.all;
library unisim;     use unisim.vcomponents.all;
library bsv;
library sdp;        use sdp.sdp.all;

architecture rtl of broken_zcu102_worker is

  -- # PS and associated CP signals
  signal ps_m_axi_gp_in   : axi.zynq_ultra_m_hp.axi_s2m_array_t(0 to C_M_AXI_HP_COUNT-1);
  signal ps_m_axi_gp_out  : axi.zynq_ultra_m_hp.axi_m2s_array_t(0 to C_M_AXI_HP_COUNT-1);
  signal ps_s_axi_hp_in   : axi.zynq_ultra_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal ps_s_axi_hp_out  : axi.zynq_ultra_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
  -- useGP1/whichGP are used in Zynq platforms but require PS/SW to detect this property's value.
  -- This is not yet supported on ZynqMP/UltraScale+, so it is tied to '0' to use GP0.
  constant whichGP        : natural := to_integer(unsigned(from_bool(useGP1)));

  -- # Reset Synchronisation Signals
  signal fclk         : std_logic_vector(3 downto 0);
  signal fclkreset_n  : std_logic;
  signal clk          : std_logic;
  signal rst_n        : std_logic; -- synchronized negative reset
  signal reset        : std_logic; -- synchronised positive reset

  -- # SDP Slave Signals
  signal my_sdp_out       : zynq_ultra_out_array_t;
  signal my_sdp_out_data  : zynq_ultra_out_data_array_t;
  signal dbg_state        : ulonglong_array_t(0 to 3);
  signal dbg_state1       : ulonglong_array_t(0 to 3);
  signal dbg_state2       : ulonglong_array_t(0 to 3);
  signal dbg_state_r      : ulonglong_array_t(0 to 3);
  signal dbg_state1_r     : ulonglong_array_t(0 to 3);
  signal dbg_state2_r     : ulonglong_array_t(0 to 3);
  
begin

  -- #######################
  -- # Zynq Ultrascale+ PS #
  -- #######################

  -- Instantiate the interface to the processor system.
  ps : zynq_ultra_ps
  port map (
    -- useGP1 is used on Zynq platforms but requires the PS/SW to detect this property's value.
    -- This is not yet suppported on ZynqMP/UltraScale, so it is tied to '0' to use GP0.
    ps_in.debug        => (31 => useGP1, others => '0'),
    ps_out.FCLK        => fclk,
    ps_out.FCLKRESET_N => fclkreset_n,
    m_axi_hp_in        => ps_m_axi_gp_in,
    m_axi_hp_out       => ps_m_axi_gp_out,
    s_axi_hp_in        => ps_s_axi_hp_in,
    s_axi_hp_out       => ps_s_axi_hp_out
  );

  -- FCLKRESET from the PS is documented as asynchronous with the associated FCLK.
  -- Here, we make a synchronised reset from it.
  clkbuf : BUFG
  port map (
    I => fclk(0),
    O => clk
  );

  sr : bsv.bsv.SyncResetA
  generic map (
    RSTDELAY => 17
  )
  port map (
    IN_RST  => fclkreset_n,
    CLK     => clk,
    OUT_RST => rst_n
  );
  reset <= not rst_n;

  -- Adapt the AXI Master from the PS to be a CP Master
  cp : axi.zynq_ultra_m_hp.axi2cp_zynq_ultra_m_hp
  port map (
    clk     => clk,
    reset   => reset,
    axi_in  => ps_m_axi_gp_out(whichGP),
    axi_out => ps_m_axi_gp_in(whichGP),
    cp_in   => cp_in,
    cp_out  => cp_out
  );

  -- Adapt the AXI Slave from the PS to be a SDP Slave
  g : for i in 0 to 3 generate
    dp : axi.zynq_ultra_s_hp.sdp2axi_zynq_ultra_s_hp
    generic map (
      ocpi_debug => true,
      sdp_width  => to_integer(sdp_width)
    )
    port map (
      clk          => clk,
      reset        => reset,
      sdp_in       => zynq_ultra_in(i),
      sdp_in_data  => zynq_ultra_in_data(i),
      sdp_out      => my_sdp_out(i),
      sdp_out_data => my_sdp_out_data(i),
      axi_in       => ps_s_axi_hp_out(i),
      axi_out      => ps_s_axi_hp_in(i),
      axi_error    => props_out.axi_error(i),
      dbg_state    => dbg_state(i),
      dbg_state1   => dbg_state1(i),
      dbg_state2   => dbg_state2(i)
    );
  end generate;
  zynq_ultra_out      <= my_sdp_out;
  zynq_ultra_out_data <= my_sdp_out_data;

  -- ##############
  -- # Properties #
  -- ##############
  props_out.dna               <= (others => '0');
  props_out.slotCardIsPresent <= (others => '0');
  props_out.memories_length   <= to_ulong(1);
  props_out.memories          <= (others => to_ulong(0));
  props_out.sdpDropCount      <= zynq_ultra_in(0).dropCount;
  props_out.debug_state       <= dbg_state_r;
  props_out.debug_state1      <= dbg_state1_r;
  props_out.debug_state2      <= dbg_state2_r;

  -- ####################################
  -- # Default OpenCPI Port Interfacing #
  -- ####################################

  -- # Metadata Port
  props_out.UUID       <= metadata_in.UUID;
  props_out.romData    <= metadata_in.romData;
  metadata_out.clk     <= clk;
  metadata_out.romAddr <= props_in.romAddr;
  metadata_out.romEn   <= props_in.romData_read;

  -- # Timebase Port
  timebase_out.clk   <= clk;
  timebase_out.reset <= reset;
  timebase_out.ppsIn <= '0';

  -- #########
  -- # Debug #
  -- #########
  work : process(clk)
  begin
    if rising_edge(clk) then
      if its(not reset) then
        dbg_state_r   <= dbg_state;
        dbg_state1_r  <= dbg_state1;
        dbg_state2_r  <= dbg_state2;
      end if;
    end if;
  end process;

end rtl;
