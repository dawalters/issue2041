# SET THIS VARIABLE to the part (die-speed-package, e.g. xc7z020-1-clg484) for the platform!
HdlPart_broken_zcu102=xczu9eg-2-ffvb1156e
# Set this variable to the names of any other component libraries with devices defined in this
# platform. Do not use slashes.  If there is an hdl/devices library in this project, it will be
# searched automatically, as will "devices" in any projects this project depends on.
# An example might be something like "our_special_devices", which would exist in this or
# other projects.
# ComponentLibraries_broken_zcu102=

