#!/usr/bin/env bash

me=$(basename "$0")
script_dir=$(dirname "$0")
cd $script_dir
script_dir=$(pwd)
echo ""
echo "Running $script_dir/$me."
echo ""

ocpidev register project
ocpidev build --hdl-platform broken_zcu102 --no-assemblies

# this should finish the assembly core, and generate the config
timeout 240s bash ocpidev build hdl assembly testbias --hdl-platform broken_zcu102

config_name=testbias_broken_zcu102_base
assy=hdl/assemblies/testbias/container-$config_name/gen/$config_name-assy.vhd

# None of our erroneous code was actually used
echo "Using grep to search for THIS_IS_A|BUNCH_OF_NONSENSE|BUT_THE_BUG_MEANS|THAT_IT_IS|ALL_IGNORED|SO_WHO_CARES"
echo "Without the bug, this should succeed with 6 matches."
cat $assy | grep "THIS_IS_A|BUNCH_OF_NONSENSE|BUT_THE_BUG_MEANS|THAT_IT_IS|ALL_IGNORED|SO_WHO_CARES"
result0=$?
echo "done"
echo ""

# The defaults were used instead
echo "Using grep to search for SFP_(REFCLK|TX|RX)"
echo "Without the bug, this should fail with 0 matches."
cat $assy | grep -E "SFP_(REFCLK|TX|RX)"
result1=$?
echo "done"
echo ""

if [ $result0 -eq 1 ]; then
  if [ $result1 -eq 0 ]; then
    echo "BUG PRESENT!"
    exit 1
  fi
else
  if [ $result1 -eq 1 ]; then
    echo "BUG NOT PRESENT!"
    exit 0
  fi
fi
